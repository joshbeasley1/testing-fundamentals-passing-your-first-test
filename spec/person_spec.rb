require_relative "../lib/person"

describe Person do
  describe "#full_name" do
    it "returns the first and last names concatenated" do
      person = Person.new(first_name: "Josh", last_name: "Beasley")

      expect(person.full_name).to eq "Josh Beasley"
    end
  end
end
